import { makeSetterName } from './mutations'
import { IClassyConstructorInternal } from './store'

/**
 * @private
 */
export function applyState (constructor: IClassyConstructorInternal, stateKeys: string[]): void {
  const prototype = constructor.prototype
  const mutableProps = constructor.__mutableProps || []

  stateKeys.forEach((k) => {
    const setterName = makeSetterName(k)
    Object.defineProperty(prototype, k, {
      configurable: true,
      enumerable: true,
      get (): any { return this.$state[k] },
      set: mutableProps.includes(k) ? function (this: any, value: any): any {
        if (this[k] !== value) this[setterName](value)
      } : undefined
    })
  })
}

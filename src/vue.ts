/**
 * Extends interfaces in Vue.js
 */

// tslint:disable interface-name

import Vue from 'vue'
import { ClassyStore } from './store'

declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    classyStore?: ClassyStore | (() => ClassyStore)
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $classyStore: ClassyStore
  }
}

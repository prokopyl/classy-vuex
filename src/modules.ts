import Vue from 'vue'
import { ModuleTree } from 'vuex'
import {
  ClassyInstance,
  ClassyInternalInstance,
  ClassyStore,
  IClassyVuexExtra,
  IModuleList, makeContexts
} from './store'

/**
 * @private
 */
export interface IModuleListInternal<T extends ClassyStore> {[k: string]: IClassyModule<T>}

export class ClassyModule<RootStore extends ClassyStore> {
  public $moduleName!: string
  public readonly $modules!: IModuleList<RootStore>

  get $root (this: IClassyModule<RootStore>): RootStore {
    if (this.$_root) return this.$_root
    this.$_root = this.$parent.$root as RootStore
    return this.$_root
  }

  get $modulePath (this: IClassyModule<RootStore>): string {
    if (this.$_modulePath) return this.$_modulePath
    this.$_modulePath = this.$moduleName + '/' + this.$parent.$modulePath
    return this.$_modulePath
  }

  get $state (this: IClassyModule<RootStore>): any {
    return this.$root.$store.state[this.$moduleName]
  }

  protected $set<T> (object: {[K: string]: T}, key: string, value: T): T
  protected $set<T> (array: T[], key: number, value: T): T

  protected $set<T> (object: any, key: any, value: T): T {
    return Vue.set(object, key, value)
  }
}

/**
 * @private
 */
export type IClassyModule<T extends ClassyStore> = ClassyModule<T> & IClassyVuexExtra & {
  $root: T
  $parent: ClassyInstance
  $set: (object: any, key: any, value: any) => any

  // tslint:disable-next-line variable-name
  $_root: T
  // tslint:disable-next-line variable-name
  $_modulePath: string
}

/**
 * @private
 */
export function extractModules (instance: ClassyStore, stateInstance: any, stateKeys: string[]): ModuleTree<any> {
  const moduleOptions: ModuleTree<any> = {}
  const modules: IModuleListInternal<any> = {}

  stateKeys.forEach((k) => {
    let obj = stateInstance[k]
    if (obj instanceof ClassyModule) {
      obj = (obj as IClassyModule<any>)
      obj.$parent = instance
      obj.$moduleName = k

      modules[k] = obj
      moduleOptions[k] = (obj as any).$__options
    }
  })

  // @ts-ignore
  instance.$modules = modules

  return moduleOptions
}

/**
 * @private
 */
export function applyModules (instance: ClassyInstance): void {
  Object.entries(instance.$modules).forEach(([k, v]) => {
    Object.defineProperty(instance, k, {
      get (): any { return v },
      configurable: true,
      enumerable: true
    })
  })
}

/**
 * @private
 */
export function initializeSubmodules (rootInstance: ClassyInternalInstance, modules: IModuleListInternal<any>): void {
  Object.entries(modules).forEach(([k, module]) => {
    module.$moduleName = k
    module.$parent = rootInstance
    makeContexts(module)
  })
}

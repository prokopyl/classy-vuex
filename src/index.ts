import { Vue as _Vue } from 'vue/types/vue'
import { Store } from 'vuex/types'
import { Action } from './actions'
import { Getter } from './getters'
import { ClassyModule } from './modules'
import { Mutable } from './mutations'
import { ClassyStore, StoreModule, use } from './store'
import './vue'

export interface IClassyVuexInstallOptions { Vuex: {Store: typeof Store} }
export default {
  use (vuex: {Store: typeof Store}): void {
    use(vuex)
  },
  install (Vue: typeof _Vue, installOptions: IClassyVuexInstallOptions): void {
    use(installOptions.Vuex)
    Vue.mixin({ beforeCreate: classyVuexInit })

    function classyVuexInit (this: _Vue): void {
      const options = this.$options
      // store injection
      if (options.classyStore) {
        this.$classyStore = typeof options.classyStore === 'function'
          ? options.classyStore()
          : options.classyStore
      } else if (options.parent && options.parent.$classyStore) {
        this.$classyStore = options.parent.$classyStore
      }

      if (this.$classyStore && !this.$store) this.$store = this.$classyStore.$store
    }
  }
}

export { Action, Getter, Mutable, ClassyModule, ClassyStore, StoreModule }

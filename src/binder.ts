import Vue from 'vue'
import { ComputedOptions } from 'vue/types/options'
import { ClassyInstance, ClassyStore } from './store'

type StoreOwn<T extends ClassyStore> = Pick<T, Exclude<keyof T, keyof ClassyStore>>

/**
 * @private
 */
export type InstanceOwn<T extends ClassyInstance> = Pick<T, Exclude<keyof T, keyof ClassyInstance>>

export type StoreMethodKeys<T extends ClassyStore> = {
  [K in keyof StoreOwn<T>]: T[K] extends (...a: any[]) => any ? K : never
}[keyof StoreOwn<T>]

type MethodDefinitions<T, K extends keyof T> = {
  [P in K]: (this: Vue, ...args: any[]) => any;
}

export type StoreComputedKeys<T extends ClassyStore> = {
  [K in keyof StoreOwn<T>]:
  T[K] extends (...a: any[]) => any
    ? T[K] extends () => any ? K : never
    : K
}[keyof StoreOwn<T>]

type ComputedDefinitions<T, K extends keyof T> = {
  [P in K]: ComputedOptions<T[P]>
}

/**
 * @private
 */
export type VueClass<V> = (new (...args: any[]) => V & Vue) & typeof Vue

function mapMethods <
  T extends ClassyStore,
  K extends StoreMethodKeys<T>
  > (prototype: any, keys: K[]): MethodDefinitions<T, K> {

  const m: MethodDefinitions<T, K> = {} as MethodDefinitions<T, K>
  keys.forEach((k) => {
    const method = prototype[k]
    if (!(method instanceof Function)) throw new TypeError('ohno')
    m[k] = function (this: Vue, ...args: any[]): any { return method.apply(this.$classyStore, args) }
  })
  return m
}

function mapComputed <
  T extends ClassyStore,
  K extends StoreComputedKeys<T>
  > (prototype: any, keys: K[]): ComputedDefinitions<T, K> {
  const m: ComputedDefinitions<T, K> = {} as ComputedDefinitions<T, K>
  keys.forEach((k) => {
    const def: ComputedOptions<any> = {}
    const desc = Object.getOwnPropertyDescriptor(prototype, k)
    if (!desc) return
    if (desc.get) {
      const getter = desc.get
      def.get = function (this: Vue): any { return getter.apply(this.$classyStore) }
    }
    if (desc.set) {
      const setter = desc.set
      def.set = function (this: Vue, v: any): void { return setter.call(this.$classyStore, v) }
    }
    if (typeof desc.value === 'function') {
      const getter = desc.value
      def.get = function (this: Vue): any { return getter.apply(this.$classyStore) }
    }
    m[k] = def
  })
  return m
}

export interface IBindParams<
  T extends ClassyStore,
  M extends StoreMethodKeys<T>,
  C extends StoreComputedKeys<T>
  > {
  methods?: M[]
  computed?: C[]
}

let checkClassyVuex: (this: Vue) => void | undefined

if (process.env.NODE_ENV !== 'production') {
  checkClassyVuex = function (this: Vue): void {
    if (!this.$classyStore) {
      throw new Error('Bound ClassyStore to a vue component without classyStore context.\n' +
        'Did you forget to Vue.use() ClassyVuex, or to set the classyStore Vue option ?')
    }
  }
}

/**
 * @private
 */
export function bind<
  T extends ClassyStore,
  M extends StoreMethodKeys<T>,
  C extends StoreComputedKeys<T>> (constructor: new () => T, params: IBindParams<T, M, C>)
: VueClass<Vue & Pick<T, M> & Pick<T, C>> {
  const prototype = Object.getPrototypeOf(constructor.prototype)
  return Vue.extend({
    beforeCreate: checkClassyVuex,
    computed: params.computed ? mapComputed<T, C>(prototype, params.computed) : undefined,
    methods: params.methods ? mapMethods<T, M>(prototype, params.methods) : undefined
  })
}

import { GetterTree } from 'vuex'
import { IModuleListInternal } from './modules'
import { ClassyInternalInstance, IClassyConstructorInternal, IClassyStore } from './store'
import { getPropertyDescriptor } from './util'

// TODO: Statically check valid targets
export function Getter (target: any, propertyKey: string): any {
  target[propertyKey].__isGetter = true
}

/**
 * @private
 */
export function makeGetterContext (instance: ClassyInternalInstance): any {
  const store = instance.$root.$store
  const storeOptions = instance.$__options
  const context = Object.create(storeOptions.state)

  if (storeOptions.getters) {
    Object.keys(storeOptions.getters).forEach((k) => {
      Object.defineProperty(context, k, {
        get (): any { return store.getters[instance.$modulePath + k] },
        configurable: true,
        enumerable: true
      })
    })
  }

  if (storeOptions.modules) {
    Object.keys(storeOptions.modules).forEach((k) => {
      Object.defineProperty(context, k, {
        get (): any { return (instance.$modules as IModuleListInternal<any>)[k].$__getterContext },
        configurable: true,
        enumerable: true
      })
    })
  }

  Object.defineProperty(context, '$root', {
    get (): any { return instance.$root.$__getterContext }
  })

  return context
}

/**
 * @private
 */
export interface IGetters {[k: string]: () => void}

/**
 * @private
 */
export function overridePrototypeGetters (constructor: IClassyConstructorInternal, prototypeKeys: string[]): IGetters {
  const prototype = constructor.prototype
  const getters: IGetters = {}
  const setters: string[] = []

  prototypeKeys.forEach((k) => {
    const desc = getPropertyDescriptor(prototype, k)
    if (desc) {
      if (typeof desc.get === 'function') {
        getters[k] = desc.get

        Object.defineProperty(prototype, k, {
          configurable: true,
          enumerable: true,
          get (): any { return this.$root.$store.getters[this.$modulePath + k] },
          set: desc.set
        })
      }
      if (typeof desc.set === 'function') {
        setters.push(k)
      }
    }

    if ((!desc) || (typeof desc!.value === 'function')) {
      const getter = (prototype as any)[k]
      if (typeof getter === 'function' && getter.__isGetter) {
        getters[k] = function (): any {
          const that = this; return function (this: any): any { return getter.apply(that, arguments) }
        }

        Object.defineProperty(prototype, k, {
          configurable: true,
          enumerable: true,
          get (): any { return this.$root.$store.getters[this.$modulePath + k] }
        })
      }
    }
  })

  constructor.__setters = setters
  return getters
}

/**
 * @private
 */
export function makeGetterTree (instance: IClassyStore, originalGetters: IGetters): GetterTree<any, any> {
  const getters: GetterTree<any, any> = {}

  Object.entries(originalGetters).forEach(([k, v]) => {
    getters[k] = () => v.apply(instance.$__getterContext)
  })

  return getters
}

import { ActionTree } from 'vuex'
import { InstanceOwn } from './binder'
import { ClassyInstance, ClassyInternalInstance, IClassyConstructorInternal, IClassyStore } from './store'

/**
 * @private
 */
export type StoreActionKeys<T extends ClassyInstance> = {
  [K in keyof InstanceOwn<T>]: T[K] extends (this: T, ...a: any[]) => (Promise<any> | void) ? K : never
}[keyof InstanceOwn<T>]

export function Action<T extends ClassyInstance, K extends StoreActionKeys<T>> (target: T, propertyKey: K): any {
  (target[propertyKey] as any).__isAction = true
}

/**
 * @private
 */
export function makeActionContext (instance: ClassyInternalInstance): any {
  return instance
}

/**
 * @private
 */
export interface IActions {[k: string]: (...params: any) => any}

/**
 * @private
 */
export function overridePrototypeActions (constructor: IClassyConstructorInternal, prototypeKeys: string[]): IActions {
  const prototype = constructor.prototype
  const originalActions: IActions = {}

  prototypeKeys.forEach((k) => {
    const method = (prototype as any)[k]
    if (typeof method === 'function' && method.__isAction) {
      (prototype as any)[k] = function (...args: any[]): any {
        return this.$root.$store.dispatch(this.$modulePath + k, args)
      }
      originalActions[k] = method
    }
  })

  return originalActions
}

/**
 * @private
 */
export function makeActionTree (instance: IClassyStore, originalActions: IActions): ActionTree<any, any> {
  const actions: ActionTree<any, any> = {}

  Object.entries(originalActions).forEach(([k, v]) => {
    actions[k] = (_, args: any) => {
      return v.apply(instance.$__actionContext, Array.isArray(args) ? args : [args])
    }
  })

  return actions
}


export function getPropertyDescriptor (object: any, name: string): PropertyDescriptor | undefined {
  do {
    const descriptor = Object.getOwnPropertyDescriptor(object, name)
    if (descriptor) return descriptor
    object = Object.getPrototypeOf(object)
  } while (object)

  return undefined
}

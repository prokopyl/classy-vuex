import Vue, { WatchOptions } from 'vue'
import { MutationPayload, Store, StoreOptions } from 'vuex/types'
import { IActions, makeActionContext, makeActionTree, overridePrototypeActions } from './actions'
import {
  bind,
  IBindParams,
  StoreComputedKeys,
  StoreMethodKeys,
  VueClass
} from './binder'
import { IGetters, makeGetterContext, makeGetterTree, overridePrototypeGetters } from './getters'
import {
  applyModules,
  ClassyModule,
  extractModules,
  IClassyModule,
  IModuleListInternal,
  initializeSubmodules
} from './modules'
import { IMutations, makeMutationContext, makeMutationTree, overridePrototypeMutations } from './mutations'
import { applyState } from './state'

export interface IModuleList<T extends ClassyStore> {[k: string]: ClassyModule<T>}

/**
 * The base class for all stores.
 */
export abstract class ClassyStore {

  get $root (): this {
    return this
  }

  get $state (): any {
    return this.$store.state
  }

  get $modulePath (): string {
    return ''
  }

  public static bind<T extends ClassyStore,
    M extends StoreMethodKeys<T>,
    C extends StoreComputedKeys<T>> (this: new () => T, params: IBindParams<T, M, C>)
    : VueClass<Vue & Pick<T, M> & Pick<T, C>> {
    return bind(this, params)
  }

  public readonly $store!: Store<any>
  public readonly $modules!: IModuleList<this>

  public $subscribe (fn: (this: this, mutation: MutationPayload) => any): () => void {
    return this.$store.subscribe((m) => fn.call(this, m))
  }

  public $watch<T> (
    getter: (this: null, store: this) => T,
    cb: (value: T, oldValue: T) => void,
    options?: WatchOptions
  ): () => void {
    return this.$store.watch(() => getter.call(null, this), cb, options)
  }

  public $registerModule (name: string, module: ClassyModule<this>): void {
    this.$modules[name] = module
    module.$moduleName = name;
    (module as IClassyModule<this>).$parent = this
    initializeSubmodules(this as any as IClassyStore, { [name]: module as IClassyModule<this> })
    this.$store.registerModule(name, (module as IClassyModule<this>).$__options)
  }

  public $waitFor (watcher: (store: this) => boolean): Promise<void> {
    return new Promise((resolve) => {
      if (watcher(this)) return resolve()
      const canceller = this.$watch(watcher, (value) => {
        if (value) {
          canceller()
          resolve()
        }
      })
    })
  }

  protected $set<T> (object: {[K: string]: T}, key: string, value: T): T
  protected $set<T> (array: T[], key: number, value: T): T

  protected $set<T> (object: any, key: any, value: T): T {
    return Vue.set(object, key, value)
  }
}

export type ClassyConstructor = new(...args: any[]) => ClassyInstance

/**
 * @private
 */
export interface IClassyVuexExtra {
  $__getterContext: any
  $__mutationContext: any
  $__actionContext: any
  $__options: StoreOptions<any>
  $__constructor: IClassyConstructorInternal
  $set: (object: any, key: any, value: any) => any
}

/**
 * @private
 */
export type IClassyStore = ClassyStore & IClassyVuexExtra

/**
 * @private
 */
export interface IClassyConstructorInternal extends ClassyConstructor {
  __mutableProps?: string[]
  __setters?: string[]
}

export type ClassyInstance = ClassyStore | ClassyModule<any>

/**
 * @private
 */
export type ClassyInternalInstance = IClassyStore | IClassyModule<any>

/**
 * @private
 */
export function makeContexts (instance: ClassyInternalInstance): void {
  instance.$__getterContext = makeGetterContext(instance)
  instance.$__mutationContext = makeMutationContext(instance)
  instance.$__actionContext = makeActionContext(instance)
}

let vuexStore: typeof Store | null = null

/**
 * @private
 */
export function use (vuex: {Store: typeof Store}): void {
  vuexStore = vuex.Store
}

export function StoreModule<T extends ClassyConstructor> (_constructor: T): T {
  const stateApplied = false
  // @ts-ignore
  const constructor: IClassyConstructorInternal = _constructor
  const originalStore = overrideStore(constructor)

  // Make constructor apply on state object directly
  // @ts-ignore
  // tslint:disable-next-line:max-classes-per-file
  return class extends _constructor {
    // @ts-ignore
    constructor (...constructorArgs: any[]) {
      // @ts-ignore
      const instance: IClassyStore = this
      initializeInstance(constructor, constructorArgs, instance, originalStore, stateApplied)
    }
  }
}

/**
 * @private
 */
interface IStore { getters: IGetters, mutations: IMutations, actions: IActions }

function getConstructorFullPrototypeKeys (constructor: IClassyConstructorInternal): string[] {
  const keys: Record<string, true> = {}
  let currentProto = constructor.prototype

  do {
    Object.keys(currentProto)
      .concat(Object.keys(Object.getOwnPropertyDescriptors(currentProto)))
      .reduce(((previousValue, currentValue) => {
        previousValue[currentValue] = true
        return previousValue
      }), keys)
    currentProto = Object.getPrototypeOf(currentProto)
  } while (currentProto !== ClassyModule.prototype && currentProto !== ClassyStore.prototype)

  return Object.keys(keys)
}

/**
 * @private
 */
function overrideStore (constructor: IClassyConstructorInternal): IStore {
  let prototypeKeys = getConstructorFullPrototypeKeys(constructor).filter((k) => k !== 'constructor')

  const getters = overridePrototypeGetters(constructor, prototypeKeys)

  const getterKeys = Object.keys(getters)
  prototypeKeys = prototypeKeys.filter((k) => !getterKeys.includes(k))

  const actions = overridePrototypeActions(constructor, prototypeKeys)
  const actionKeys = Object.keys(actions)
  prototypeKeys = prototypeKeys.filter((k) => !actionKeys.includes(k))

  const mutations = overridePrototypeMutations(constructor, prototypeKeys)

  return { getters, mutations, actions }
}

/**
 * @private
 */
function makeStoreOptions (instance: IClassyStore, originalStore: IStore): StoreOptions<any> {
  return {
    actions: makeActionTree(instance, originalStore.actions),
    getters: makeGetterTree(instance, originalStore.getters),
    mutations: makeMutationTree(instance, originalStore.mutations)
  }
}

let instanciating = 0
function sanityChecks (instance: any): void {
  if (instance instanceof ClassyStore && !vuexStore) {
    throw new Error('[classy-vuex] You must call ClassyStore.use(Vuex) before creating a new store')
  }
}

function initializeInstance (
  constructor: IClassyConstructorInternal,
  constructorArgs: any[],
  instance: IClassyStore,
  originalStore: IStore,
  stateApplied: boolean
): void {
  sanityChecks(instance)
  ++instanciating
  instance.$__constructor = constructor
  const state = constructor.apply({}, constructorArgs) as any
  let stateKeys = Object.keys(state).filter((k) => state.hasOwnProperty(k))

  const moduleOptions = extractModules(instance, state, stateKeys)
  const modules = instance.$modules
  const moduleKeys = Object.keys(modules)
  stateKeys.filter((k) => moduleKeys.includes(k)).forEach((k) => { delete state[k] })
  stateKeys = stateKeys.filter((k) => !moduleKeys.includes(k))

  if (!stateApplied) {
    applyState(constructor, stateKeys)
  }

  applyModules(instance)

  const storeOptions = makeStoreOptions(instance, originalStore)
  const options = { namespaced: true, state, modules: moduleOptions, ...storeOptions }

  instance.$__options = options

  if (instanciating === 1 && instance instanceof ClassyStore) {
    // @ts-ignore
    instance.$store = new vuexStore!(options)
    makeContexts(instance)
    initializeSubmodules(instance, modules as IModuleListInternal<any>)
  }

  --instanciating
}

import { MutationTree } from 'vuex'
import { IModuleListInternal } from './modules'
import { ClassyInternalInstance, IClassyConstructorInternal, IClassyStore } from './store'

// TODO: Statically check valid targets
export function Mutable (target: any, propertyKey: string): void {
  const constructor = target.constructor
  const mutableProps = constructor.__mutableProps || (constructor.__mutableProps = [])
  mutableProps.push(propertyKey)
  target[makeSetterName(propertyKey)] = function (v: any): void { this[propertyKey] = v } // TODO:
}

/**
 * @private
 */
export function makeSetterName (propertyName: string): string {
  return 'set' + propertyName.slice(0,1).toUpperCase() + propertyName.slice(1)
}

/**
 * @private
 */
export function makeMutationContext (instance: ClassyInternalInstance): any {
  const storeOptions = instance.$__options
  const getterContext = instance.$__getterContext
  const setters = instance.$__constructor.__setters
  const settersNames = setters!.map(makeSetterName)
  const context = Object.create(getterContext)

  if (storeOptions.mutations) {
    Object.keys(storeOptions.mutations).forEach((k) => {
      const setterIndex = settersNames.indexOf(k)
      if (setterIndex >= 0) {
        const propertyName = setters![setterIndex]
        const desc = Object.getOwnPropertyDescriptor(context, k)
        Object.defineProperty(context, propertyName, {
          configurable: true,
          enumerable: true,
          get: desc ? desc.get : undefined,
          set (v: any): void { return storeOptions.mutations![k].apply(context, [undefined, v]) }
        })
      } else {
        context[k] = (...args: any[]) => storeOptions.mutations![k].apply(context, [instance.$__options.state, args])
      }
    })
  }

  if (storeOptions.modules) {
    Object.keys(storeOptions.modules).forEach((k) => {
      Object.defineProperty(context, k, {
        get (): any { return (instance.$modules as IModuleListInternal<any>)[k].$__mutationContext },
        configurable: true,
        enumerable: true
      })
    })
  }

  Object.defineProperty(context, '$root', {
    get (): any { return instance.$root.$__mutationContext }
  })

  context.$set = instance.$set.bind(instance)

  return context
}

/**
 * @private
 */
export interface IMutations {[k: string]: (...params: any) => any}

/**
 * @private
 */
export function overridePrototypeMutations (
  constructor: IClassyConstructorInternal,
  prototypeKeys: string[]
): IMutations {

  const prototype = constructor.prototype
  const setters = constructor.__setters
  const originalMutations: IMutations = {}

  prototypeKeys.forEach((k) => {
    const method = (prototype as any)[k]
    if (typeof method === 'function') {
      (prototype as any)[k] = function (...args: any[]): void {
        return this.$root.$store.commit(this.$modulePath + k, args)
      }
      originalMutations[k] = method
    }
  })

  setters!.forEach((k) => {
    const desc = Object.getOwnPropertyDescriptor(prototype, k)!
    const setterName = makeSetterName(k)
    originalMutations[setterName] = desc.set!
    Object.defineProperty(prototype, k, {
      get: desc.get,
      set (v: any): void { return this.$root.$store.commit(this.$modulePath + setterName, v) }
    })
  })

  return originalMutations
}

/**
 * @private
 */
export function makeMutationTree (instance: IClassyStore, originalMutations: IMutations): MutationTree<any> {
  const mutations: MutationTree<any> = {}

  Object.entries(originalMutations).forEach(([k, v]) => {
    mutations[k] = (_, args: any) => v.apply(instance.$__mutationContext, Array.isArray(args) ? args : [args])
  })

  return mutations
}

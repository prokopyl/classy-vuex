import { Action, ClassyStore, Mutable, StoreModule } from '../src'
import { defaultStoreOptions } from './index'

/* tslint:disable:no-unused-expression no-empty */

describe('actions', () => {

  @StoreModule
  class Store extends ClassyStore {
    public foo = 42
    @Mutable public bar = 42

    get fooGetter (): number { return this.foo + 10 }
    public fooMutation (): void { this.foo = 69 }

    @Action public async basicAction (): Promise<number> { return Promise.resolve(20) }
    @Action public async fooAction (): Promise<number> { return Promise.resolve(this.foo) }
    @Action public async fooGetterAction (): Promise<number> { return Promise.resolve(this.fooGetter) }
    @Action public async badFooAction (): Promise<void> { this.foo = 5; return Promise.resolve() }
    @Action public async fooMutationAction (): Promise<void> { this.fooMutation(); return Promise.resolve() }
    @Action public async barMutable (): Promise<void> { this.bar = await Promise.resolve(69) }
    @Action public async paramsAction (a: number, b: number): Promise<number> { return Promise.resolve(a + b) }
    @Action public async fooFooAction (): Promise<number> { return (await this.fooAction()) + 10 }
  }

  let store: Store
  let mutations: any[] = []

  beforeEach(() => {
    store = new Store()
    mutations = []
    store.$store.subscribe((m) => mutations.push(m))
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      actions: {
        badFooAction: expect.any(Function),
        barMutable: expect.any(Function),
        basicAction: expect.any(Function),
        fooAction: expect.any(Function),
        fooFooAction: expect.any(Function),
        fooGetterAction: expect.any(Function),
        fooMutationAction: expect.any(Function),
        paramsAction: expect.any(Function)
      },
      getters: { fooGetter: expect.any(Function) },
      mutations: { fooMutation: expect.any(Function), setBar: expect.any(Function) },
      state: { foo: 42, bar: 42 }
    })
  })

  it('can be called', async () => {
    await expect(store.basicAction()).resolves.toBe(20)
  })

  it('can access state', async () => {
    await expect(store.fooAction()).resolves.toBe(42)
  })

  it('cannot directly mutate state', async () => {
    await expect(store.badFooAction()).rejects.toBeInstanceOf(TypeError)
  })

  it('can access getters', async () => {
    await expect(store.fooGetterAction()).resolves.toBe(42 + 10)
  })

  it('can trigger other mutations', async () => {
    await expect(store.fooMutationAction()).resolves.toBeUndefined()
    expect(store.foo).toBe(69)
  })

  it('can trigger auto-generated mutations from mutable properties', async () => {
    await store.barMutable()
    expect(store.bar).toBe(69)
  })

  it('can receive parameters', async () => {
    await expect(store.paramsAction(2, 6)).resolves.toBe(8)
  })

  it('can call other actions', async () => {
    await expect(store.fooFooAction()).resolves.toBe(42 + 10)
  })
})

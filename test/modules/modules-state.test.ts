import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('modules state', () => {

  @StoreModule
  class FooModule extends ClassyModule<Store> {
    public foo = 42
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public rootFoo = 69
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          state: { foo: 42 }
        }
      },
      state: {
        fooModule: expect.anything(),
        rootFoo: 69
      }
    })
  })

  it('can access state in submodule', () => {
    expect(store.fooModule.foo).toBe(42)
  })

  it('can access root state from submodule', () => {
    expect(store.fooModule.$root.rootFoo).toBe(69)
  })
})

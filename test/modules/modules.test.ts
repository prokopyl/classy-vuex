import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('modules management', () => {
  // TODO: test more than one module
  @StoreModule
  class FooModule extends ClassyModule<Store> {}

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: defaultStoreOptions
      },
      state: { fooModule: expect.anything() }
    })
  })

  it('has a $root property', () => {
    expect(store.fooModule.$root).toBe(store)
  })

  it('has a $moduleName property', () => {
    expect(store.fooModule.$moduleName).toEqual('fooModule')
  })
})

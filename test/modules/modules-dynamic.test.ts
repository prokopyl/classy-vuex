import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('dynamic modules management', () => {
  @StoreModule
  class FooModule extends ClassyModule<Store> {
    get getter (): number {
      return this.$root.state + 20
    }

    public setRootState (): void {
      this.$root.setState(86)
    }
  }

  @StoreModule
  class Store extends ClassyStore {
    public state: number = 42

    public setState (value: number): void {
      this.state = value
    }
  }

  let store: Store
  let foo: FooModule

  beforeEach(() => {
    store = new Store()
    foo = new FooModule()
    store.$registerModule('foo', foo)
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      state: { state: 42, foo: {} },
      mutations: {
        setState: expect.any(Function)
      }
    })
  })

  it('has a $root property', () => {
    expect(foo.$root).toBe(store)
  })

  it('has a $moduleName property', () => {
    expect(foo.$moduleName).toEqual('foo')
  })

  it('can read from root store', () => {
    expect(store.$store.getters['foo/getter']).toBe(62)
    expect(foo.getter).toBe(62)
  })

  it('can mutate root store', () => {
    expect(store.state).toBe(42)
    foo.setRootState()
    expect(store.$store.getters['foo/getter']).toBe(106)
    expect(store.state).toBe(86)
  })
})

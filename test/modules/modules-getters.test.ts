import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('modules management', () => {

  @StoreModule
  class FooModule extends ClassyModule<Store> {
    public foo = 42

    get fooGetter (): number { return this.foo + 10 }
    get fooInFooGetter (): number { return this.fooGetter + 10 }
    get rootFooGetter (): number { return this.$root.rootFoo + 10 }
    get rootRootFooGetter (): number { return this.$root.rootFooGetter + 10 }
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public rootFoo = 69

    get rootFooGetter (): number { return this.rootFoo + 10 }
    get fooModuleGetter (): number { return this.fooModule.foo + 10 }
    get fooModuleFooGetter (): number { return this.fooModule.fooGetter + 10 }
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      getters: {
        fooModuleFooGetter: expect.any(Function),
        fooModuleGetter: expect.any(Function),
        rootFooGetter: expect.any(Function)
      },
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          getters: {
            fooGetter: expect.any(Function),
            fooInFooGetter: expect.any(Function),
            rootFooGetter: expect.any(Function),
            rootRootFooGetter: expect.any(Function)
          },
          state: { foo: 42 }
        }
      },
      state: {
        fooModule: expect.anything(),
        rootFoo: 69
      }
    })
  })

  it('can access getters in submodule', () => {
    expect(store.$store.getters['fooModule/fooGetter']).toBe(42 + 10)
    expect(store.fooModule.fooGetter).toBe(42 + 10)
  })

  it('can access getters accessing getters in submodule', () => {
    expect(store.fooModule.fooInFooGetter).toBe(42 + 10 + 10)
  })

  it('can access root state in submodule', () => {
    expect(store.fooModule.rootFooGetter).toBe(69 + 10)
  })

  it('can access root getters in submodule', () => {
    expect(store.fooModule.rootFooGetter).toBe(69 + 10)
  })

  it('can access submodule state from root', () => {
    expect(store.fooModuleGetter).toBe(42 + 10)
  })

  it('can access submodule getters from root', () => {
    expect(store.fooModuleFooGetter).toBe(42 + 10 + 10)
  })

  it('can access submodule getters accessing root getters', () => {
    expect(store.fooModule.rootRootFooGetter).toBe(69 + 10 + 10)
  })

})

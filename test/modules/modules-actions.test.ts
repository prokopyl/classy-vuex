import { Action, ClassyModule, ClassyStore, Mutable, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('modules management', () => {

  @StoreModule
  class FooModule extends ClassyModule<Store> {
    public foo = 42
    @Mutable public bar = 42

    get fooGetter (): number { return this.foo + 10 }
    public fooMutation (): void { this.foo = 69 }

    @Action public async fooAction (): Promise<number> { return this.foo }

    @Action public async fooGetterAction (): Promise<number> { return this.fooGetter }
    @Action public async rootFooGetterAction (): Promise<number> { return this.$root.rootFooGetter }

    @Action public async fooMutationAction (): Promise<void> { this.fooMutation() }
    @Action public async rootFooMutationAction (): Promise<void> { this.$root.rootFooMutation() }

    @Action public async badFooAction (): Promise<void> { this.foo = 5 }
    @Action public async barMutable (): Promise<void> { this.bar = await Promise.resolve(69) }

    @Action public async paramsAction (a: number, b: number): Promise<number> { return a + b }
    @Action public async fooFooAction (): Promise<number> { return (await this.fooAction()) + 10 }
    @Action public async rootFooAction (): Promise<number> { return (await this.$root.rootFooAction()) + 10 }
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public rootFoo = 69

    get rootFooGetter (): number { return this.rootFoo + 10 }
    public rootFooMutation (): void { this.rootFoo = 42 }
    @Action public rootFooAction (): Promise<number> { return Promise.resolve(66) }
    @Action public fooModuleFooAction (): Promise<number> { return Promise.resolve(66) }
  }

  let store: Store
  let mutations: any[] = []

  beforeEach(() => {
    store = new Store()
    mutations = []
    store.$store.subscribe((m) => mutations.push(m))
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      actions: {
        fooModuleFooAction: expect.any(Function),
        rootFooAction: expect.any(Function)
      },
      getters: { rootFooGetter: expect.any(Function) },
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          actions: {
            badFooAction: expect.any(Function),
            barMutable: expect.any(Function),
            fooAction: expect.any(Function),
            fooFooAction: expect.any(Function),
            fooGetterAction: expect.any(Function),
            fooMutationAction: expect.any(Function),
            paramsAction: expect.any(Function),
            rootFooAction: expect.any(Function),
            rootFooGetterAction: expect.any(Function),
            rootFooMutationAction: expect.any(Function)
          },
          getters: { fooGetter: expect.any(Function) },
          mutations: {
            fooMutation: expect.any(Function),
            setBar: expect.any(Function)
          },
          state: { foo: 42, bar: 42 }
        }
      },
      mutations: { rootFooMutation: expect.any(Function) },
      state: {
        fooModule: expect.anything(),
        rootFoo: 69
      }
    })
  })

  it('can access submodule state', async () => {
    await expect(store.fooModule.fooAction()).resolves.toBe(42)
  })

  it('cannot directly mutate submodule state', async () => {
    await expect(store.fooModule.badFooAction()).rejects.toBeInstanceOf(TypeError)
  })

  it('can access submodule getters', async () => {
    await expect(store.fooModule.fooGetterAction()).resolves.toBe(42 + 10)
  })

  it('can access root getters', async () => {
    await expect(store.fooModule.rootFooGetterAction()).resolves.toBe(69 + 10)
  })

  it('can trigger other submodule mutations', async () => {
    await expect(store.fooModule.fooMutationAction()).resolves.toBeUndefined()
    expect(store.fooModule.foo).toBe(69)
  })

  it('can trigger root mutations', async () => {
    await expect(store.fooModule.rootFooMutationAction()).resolves.toBeUndefined()
    expect(store.rootFoo).toBe(42)
  })

  it('can trigger auto-generated mutations from mutable properties', async () => {
    await store.fooModule.barMutable()
    expect(store.fooModule.bar).toBe(69)
  })

  it('can receive parameters', async () => {
    await expect(store.fooModule.paramsAction(2, 6)).resolves.toBe(8)
  })

  it('can call other submodule actions', async () => {
    await expect(store.fooModule.fooFooAction()).resolves.toBe(42 + 10)
  })

  it('can call root actions', async () => {
    await expect(store.fooModule.rootFooAction()).resolves.toBe(66 + 10)
  })
})

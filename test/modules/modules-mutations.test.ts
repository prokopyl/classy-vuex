import { ClassyModule, ClassyStore, Mutable, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('modules management', () => {

  @StoreModule
  class FooModule extends ClassyModule<Store> {
    public foo = 42
    @Mutable public bar = 42
    get fooGetter (): number { return this.foo + 10 }

    public fooMutation (): void { this.foo = 69 }
    public rootFooMutation (): void { this.$root.rootFoo = 21 }
    public fooGetterMutation (): void { this.foo = this.fooGetter }
    public fooRootGetterMutation (): void { this.foo = this.$root.rootFooGetter }

    public barMutation (): void { this.bar = 21 }

    public fooMutationInBarMutation (): void { this.fooMutation(); this.bar = 20 }
    public fooRootMutationInBarMutation (): void { this.$root.rootFooMutation(); this.bar = 20 }
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public rootFoo = 69

    get rootFooGetter (): number { return this.rootFoo + 10 }
    public rootFooMutation (): void { this.rootFoo = 42 }
    public fooModuleFooMutation (): void { this.fooModule.fooMutation() }
  }

  let store: Store
  let mutations: any[] = []

  beforeEach(() => {
    store = new Store()
    mutations = []
    store.$store.subscribe((m) => mutations.push(m))
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      getters: { rootFooGetter: expect.any(Function) },
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          getters: { fooGetter: expect.any(Function) },
          mutations: {
            barMutation: expect.any(Function),
            fooGetterMutation: expect.any(Function),
            fooMutation: expect.any(Function),
            fooMutationInBarMutation: expect.any(Function),
            fooRootGetterMutation: expect.any(Function),
            fooRootMutationInBarMutation: expect.any(Function),
            rootFooMutation: expect.any(Function),
            setBar: expect.any(Function)
          },
          state: { foo: 42, bar: 42 }
        }
      },
      mutations: {
        fooModuleFooMutation: expect.any(Function),
        rootFooMutation: expect.any(Function)
      },
      state: {
        fooModule: expect.anything(),
        rootFoo: 69
      }
    })
  })

  it('updates submodule state', () => {
    store.fooModule.fooMutation()
    expect(store.fooModule.foo).toBe(69)
    expect(store.fooModule.fooGetter).toBe(69 + 10)
    expect(mutations).toEqual([{ payload: [], type: 'fooModule/fooMutation' }])
  })

  it('updates root state', () => {
    store.fooModule.rootFooMutation()
    expect(store.rootFoo).toBe(21)
    expect(mutations).toEqual([{ payload: [], type: 'fooModule/rootFooMutation' }])

  })

  it('can be commited manually', () => {
    store.$store.commit('fooModule/fooMutation')
    expect(store.fooModule.foo).toBe(69)
    expect(store.fooModule.fooGetter).toBe(69 + 10)
  })

  it('can use submodule getters', () => {
    store.fooModule.fooGetterMutation()
    expect(store.fooModule.foo).toBe(42 + 10)
    expect(mutations).toEqual([{ payload: [], type: 'fooModule/fooGetterMutation' }])
  })

  it('can use root getters', () => {
    store.fooModule.fooRootGetterMutation()
    expect(store.fooModule.foo).toBe(69 + 10)
    expect(mutations).toEqual([{ payload: [], type: 'fooModule/fooRootGetterMutation' }])
  })

  it('can trigger other mutations', () => {
    store.fooModule.fooMutationInBarMutation()
    expect(store.fooModule.foo).toBe(69)
    expect(store.fooModule.bar).toBe(20)
    expect(mutations).toEqual([
      { payload: [], type: 'fooModule/fooMutationInBarMutation' }
    ])
  })

  it('can trigger root mutations', () => {
    store.fooModule.fooRootMutationInBarMutation()
    expect(store.rootFoo).toBe(42)
    expect(store.fooModule.bar).toBe(20)
    expect(mutations).toEqual([
      { payload: [], type: 'fooModule/fooRootMutationInBarMutation' }
    ])
  })

  it('can trigger submodule mutations', () => {
    store.fooModuleFooMutation()
    expect(store.fooModule.foo).toBe(69)
    expect(mutations).toEqual([
      { payload: [], type: 'fooModuleFooMutation' }
    ])
  })

  it('can trigger auto-generated mutations from submodule mutable properties', () => {
    store.fooModule.bar = 69
    expect(store.fooModule.bar).toBe(69)
    expect(mutations).toEqual([{ payload: [69], type: 'fooModule/setBar' }])
  })

})

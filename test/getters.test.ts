import { ClassyStore, Getter, StoreModule } from '../src'
import { defaultStoreOptions } from './index'

/* tslint:disable:no-unused-expression */

describe('getters', () => {

  let count = 0

  @StoreModule
  class Store extends ClassyStore {
    public foo = 42

    get fooGetter (): number { return this.foo + 10 }
    get fooInFooGetter (): number { return this.fooGetter + 10 }
    get expensiveGetter (): number { ++count; return this.foo }
    get selfRootGetter (): number { return this.$root.fooGetter - this.$root.foo }

    @Getter public paramsFooGetter (x: number): number { return this.foo + x }
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
    count = 0
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      getters: {
        expensiveGetter: expect.any(Function),
        fooGetter: expect.any(Function),
        fooInFooGetter: expect.any(Function),
        paramsFooGetter: expect.any(Function),
        selfRootGetter: expect.any(Function)
      },
      state: { foo: 42 }
    })
  })

  it('reflects current state', () => {
    expect(store.fooGetter).toBe(42 + 10)
    expect(store.$store.getters.fooGetter).toBe(42 + 10)
  })

  it('reflects behind-the-scene changes', () => {
    store.$store.state.foo = 21
    expect(store.fooGetter).toBe(21 + 10)
  })

  it('can use other getters directly', () => {
    expect(store.fooInFooGetter).toBe(42 + 10 + 10)
    store.$store.state.foo = 21
    expect(store.fooInFooGetter).toBe(21 + 10 + 10)
  })

  it('can receive params', () => {
    expect(store.paramsFooGetter(22)).toBe(42 + 22)
  })

  it('can access $root', () => {
    expect(store.selfRootGetter).toBe(10)
  })

  it('hits vuex cache for getters', () => {
    expect(count).toBe(0)

    store.expensiveGetter
    expect(count).toBe(1)

    store.expensiveGetter
    expect(count).toBe(1)

    store.$store.state.foo = 0
    store.expensiveGetter
    expect(count).toBe(2)
  })
})

import Vue from 'vue'
import { MutationPayload } from 'vuex'
import { ClassyStore, Mutable, StoreModule } from '../src'
import { defaultStoreOptions } from './index'

describe('state management', () => {

  @StoreModule
  class Store extends ClassyStore {
    @Mutable public foo = 42
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      mutations: { setFoo: expect.any(Function) },
      state: { foo: 42 }
    })
  })

  it('correctly calls the subscribe function with payload info', () => {
    let x = 0
    store.$subscribe(function (m: MutationPayload): void {
      expect(m.type).toEqual('setFoo')
      expect(m.payload).toEqual([69])
      expect(this.foo).toBe(69)
      x = 42
    })

    store.foo = 69
    expect(x).toBe(42)
  })

  it('correctly calls the watch callback function when changes are made', async () => {
    let x = 0
    store.$watch(($store) => $store.foo, (newVal, oldVal) => {
      expect(oldVal).toEqual(42)
      expect(newVal).toEqual(69)
      x = 42
    })

    store.foo = 69
    await Vue.nextTick()
    expect(x).toBe(42)
  })

  it('correctly resolves the waitFor promise', async () => {
    let x = 0
    store.$waitFor((s) => s.foo === 69).then(() => x = 42).catch((e) => { throw e })

    store.foo = 99
    await Vue.nextTick()
    expect(x).toBe(0)

    store.foo = 69
    await Vue.nextTick()
    expect(x).toBe(42)
  })

  it('instantly resolves the waitFor promise if condition is met', async () => {
    let x = 0
    store.$waitFor((s) => s.foo === 42).then(() => x = 42).catch((e) => { throw e })
    await Vue.nextTick()
    expect(x).toBe(42)
  })
})

import Component from 'vue-class-component'
import { ClassyStore, StoreModule } from '../src'
import './index'

// tslint:disable:max-classes-per-file

describe('binder', () => {

  @StoreModule
  class Store extends ClassyStore {
    public foo = 42

    get fooGetter (): number { return this.foo + 10 }
    public fooMutation (): void { this.foo += 10 }
  }

  @Component
  class VueComponent extends Store.bind({ computed: ['fooGetter'], methods: ['fooMutation'] }) {
    // tslint:disable-next-line no-empty
    public render (): void {}

    public myMethod (): number {
      return 42
    }
  }

  let store: Store
  let comp: VueComponent

  beforeEach(() => {
    store = new Store()
    comp = new VueComponent({ classyStore: store }).$mount()
  })

  it('binds correctly', () => {
    expect(comp.$classyStore).toBe(store)
  })

  it('can apply store getters', () => {
    expect(comp.fooGetter).toBe(52)
  })

  it('can apply store methods', () => {
    comp.fooMutation()
    expect(store.foo).toBe(52)
  })
})

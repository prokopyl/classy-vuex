import Vue from 'vue'
import Vuex from 'vuex'
import ClassyVuex from '../src'
Vue.use(Vuex)
Vue.use(ClassyVuex, { Vuex })

export const defaultStoreOptions = {
  actions: {},
  getters: {},
  modules: {},
  mutations: {},
  namespaced: true,
  state: {}
}

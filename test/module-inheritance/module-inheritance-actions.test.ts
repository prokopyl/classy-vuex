import { Action, ClassyModule, ClassyStore, Mutable, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('inherited modules actions', () => {

  class BaseModule<RootStore extends ClassyStore> extends ClassyModule<RootStore> {
    public baseFoo = 42
    @Mutable public baseMutableFoo = 69

    @Action public async doFoo (): Promise<number> {
      return this.baseFoo + 2
    }
  }

  @StoreModule
  class FooModule extends BaseModule<Store> {}

  @StoreModule
  class BarModule extends BaseModule<Store> {
    @Action public async doFoo (): Promise<number> {
      const value = await super.doFoo()
      this.baseMutableFoo = 21
      return value + 3
    }
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public barModule = new BarModule()
  }

  let store: Store
  let mutations: any[] = []

  beforeEach(() => {
    store = new Store()
    mutations = []
    store.$store.subscribe((m) => mutations.push(m))
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          actions: {
            doFoo: expect.any(Function)
          },
          mutations: {
            setBaseMutableFoo: expect.any(Function)
          },
          state: { baseFoo: 42, baseMutableFoo: 69 }
        },
        barModule: {
          ...defaultStoreOptions,
          actions: {
            doFoo: expect.any(Function)
          },
          mutations: {
            setBaseMutableFoo: expect.any(Function)
          },
          state: { baseFoo: 42, baseMutableFoo: 69 }
        }
      },
      state: {
        fooModule: { baseFoo: 42, baseMutableFoo: 69 },
        barModule: { baseFoo: 42, baseMutableFoo: 69 }
      }
    })
  })

  it('can run actions from inherited modules', async () => {
    const val = await store.fooModule.doFoo()
    expect(val).toBe(44)
    expect(store.fooModule.baseFoo).toBe(42)
    expect(store.barModule.baseFoo).toBe(42)

    expect(mutations).toEqual([])
  })

  it('can run actions from inherited modules', async () => {
    expect(store.fooModule.baseFoo).toBe(42)
    expect(store.barModule.baseFoo).toBe(42)

    const val = await store.barModule.doFoo()
    expect(val).toBe(47)
    expect(store.fooModule.baseFoo).toBe(42)
    expect(store.barModule.baseFoo).toBe(42)

    expect(mutations).toEqual([{ payload: [21], type: 'barModule/setBaseMutableFoo' }])
  })
})

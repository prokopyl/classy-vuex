import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('inherited modules state', () => {
  class BaseModule<RootStore extends ClassyStore> extends ClassyModule<RootStore> {
    public baseFoo = 42
  }

  @StoreModule
  class FooModule extends BaseModule<Store> {}

  @StoreModule
  class BarModule extends BaseModule<Store> {
    public baseFoo = 69
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public barModule = new BarModule()
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          state: { baseFoo: 42 }
        },
        barModule: {
          ...defaultStoreOptions,
          state: { baseFoo: 69 }
        }
      },
      state: {
        fooModule: expect.anything(),
        barModule: expect.anything()
      }
    })
  })

  it('can access state in submodule', () => {
    expect(store.fooModule.baseFoo).toBe(42)
    expect(store.barModule.baseFoo).toBe(69)
  })

  it('reflects behind-the-scenes changes in submodule', () => {
    store.$store.state.fooModule.baseFoo = 21
    expect(store.fooModule.baseFoo).toBe(21)
    expect(store.barModule.baseFoo).toBe(69)
  })
})

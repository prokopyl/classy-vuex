import { ClassyModule, ClassyStore, Mutable, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('inherited modules mutations', () => {

  class BaseModule<RootStore extends ClassyStore> extends ClassyModule<RootStore> {
    public baseFoo = 42
    @Mutable public baseMutableFoo = 69

    public mutateFoo (): void {
      this.baseFoo = 21
    }
  }

  @StoreModule
  class FooModule extends BaseModule<Store> {}

  @StoreModule
  class BarModule extends BaseModule<Store> {
    public bar = 0

    public mutateFoo (): void {
      super.mutateFoo()
      this.bar = 21
    }
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public barModule = new BarModule()
  }

  let store: Store
  let mutations: any[] = []

  beforeEach(() => {
    store = new Store()
    mutations = []
    store.$store.subscribe((m) => mutations.push(m))
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          mutations: {
            mutateFoo: expect.any(Function),
            setBaseMutableFoo: expect.any(Function)
          },
          state: { baseFoo: 42, baseMutableFoo: 69 }
        },
        barModule: {
          ...defaultStoreOptions,
          mutations: {
            mutateFoo: expect.any(Function),
            setBaseMutableFoo: expect.any(Function)
          },
          state: { baseFoo: 42, baseMutableFoo: 69, bar: 0 }
        }
      },
      state: {
        fooModule: { baseFoo: 42, baseMutableFoo: 69 },
        barModule: { baseFoo: 42, baseMutableFoo: 69, bar: 0 }
      }
    })
  })

  it('can run mutations from inherited modules', () => {
    store.fooModule.mutateFoo()
    expect(store.fooModule.baseFoo).toBe(21)
    expect(store.barModule.baseFoo).toBe(42)

    expect(mutations).toEqual([{ payload: [], type: 'fooModule/mutateFoo' }])
  })

  it('can run mutations from inherited', () => {
    expect(store.fooModule.baseFoo).toBe(42)
    expect(store.barModule.baseFoo).toBe(42)
    expect(store.barModule.bar).toBe(0)

    store.barModule.mutateFoo()
    expect(store.fooModule.baseFoo).toBe(42)
    expect(store.barModule.baseFoo).toBe(21)
    expect(store.barModule.bar).toBe(21)

    expect(mutations).toEqual([{ payload: [], type: 'barModule/mutateFoo' }])
  })
})

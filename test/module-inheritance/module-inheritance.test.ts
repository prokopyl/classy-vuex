import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('inherited modules', () => {
  class BaseModule<RootStore extends ClassyStore> extends ClassyModule<RootStore> {}

  @StoreModule
  class FooModule extends BaseModule<Store> {}

  @StoreModule
  class BarModule extends BaseModule<Store> {}

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public barModule = new BarModule()
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: defaultStoreOptions,
        barModule: defaultStoreOptions
      },
      state: { fooModule: {}, barModule: {} }
    })
  })

  it('has a $root property', () => {
    expect(store.fooModule.$root).toBe(store)
    expect(store.barModule.$root).toBe(store)
  })

  it('has a $moduleName property', () => {
    expect(store.fooModule.$moduleName).toEqual('fooModule')
    expect(store.barModule.$moduleName).toEqual('barModule')
  })
})

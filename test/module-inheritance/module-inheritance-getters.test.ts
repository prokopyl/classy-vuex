import { ClassyModule, ClassyStore, StoreModule } from '../../src'
import { defaultStoreOptions } from '../index'

describe('inherited modules getters', () => {
  class BaseModule<RootStore extends ClassyStore> extends ClassyModule<RootStore> {
    public baseFoo = 42

    public get baseFooGetter (): number { return this.baseFoo + 10 }
    public get baseFooInBaseFooGetter (): number { return this.baseFooGetter + 5 }
  }

  @StoreModule
  class FooModule extends BaseModule<Store> {
    public get baseFooGetter (): number { return this.baseFoo + 20 }
  }

  @StoreModule
  class BarModule extends BaseModule<Store> {
    public baseFoo = 69
  }

  @StoreModule
  class Store extends ClassyStore {
    public fooModule = new FooModule()
    public barModule = new BarModule()
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      modules: {
        fooModule: {
          ...defaultStoreOptions,
          getters: {
            baseFooGetter: expect.any(Function),
            baseFooInBaseFooGetter: expect.any(Function)
          },
          state: { baseFoo: 42 }
        },
        barModule: {
          ...defaultStoreOptions,
          getters: {
            baseFooGetter: expect.any(Function),
            baseFooInBaseFooGetter: expect.any(Function)
          },
          state: { baseFoo: 69 }
        }
      },
      state: {
        fooModule: { baseFoo: 42 },
        barModule: { baseFoo: 69 }
      }
    })
  })

  it('can access overriden getters in submodule', () => {
    expect(store.$store.getters['fooModule/baseFooGetter']).toBe(42 + 20)
    expect(store.fooModule.baseFooGetter).toBe(42 + 20)

    expect(store.barModule.baseFooGetter).toBe(69 + 10)
  })

  it('can access getters accessing overriden getters in submodule', () => {
    expect(store.fooModule.baseFooInBaseFooGetter).toBe(42 + 20 + 5)
    expect(store.barModule.baseFooInBaseFooGetter).toBe(69 + 10 + 5)
  })
})

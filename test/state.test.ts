import { ClassyStore, StoreModule } from '../src'
import { defaultStoreOptions } from './index'

describe('state management', () => {

  @StoreModule
  class Store extends ClassyStore {
    public foo = 42
  }

  let store: Store

  beforeEach(() => {
    store = new Store()
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      state: { foo: 42 }
    })
  })

  it('reflects current state', () => {
    expect(store.foo).toBe(42)
    expect(store.$store.state.foo).toBe(42)
  })

  it('reflects behind-the-scene changes', () => {
    store.$store.state.foo = 21
    expect(store.foo).toBe(21)
  })

  it('prevents state to be set directly', () => {
    expect(() => store.foo = 0).toThrow(TypeError)
  })
})

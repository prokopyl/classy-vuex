import { ClassyStore, Mutable, StoreModule } from '../src'
import { defaultStoreOptions } from './index'

/* tslint:disable:no-unused-expression no-empty */

describe('mutations', () => {

  @StoreModule
  class Store extends ClassyStore {
    public foo = 42
    @Mutable public bar = 42

    get fooGetter (): number { return this.foo + 10 }

    public fooMutation (): void { this.foo = 69 }
    public fooGetterMutation (): void { this.foo = this.fooGetter }
    public fooParamMutation (a: number, b: number, c: number): void { this.foo = a + b + c }

    public barMutation (): void { this.bar = 21 }
    set fooSetter (val: number) { this.foo = val + 10 }

    get baz (): number { return this.bar }
    set baz (v: number) { this.bar = v }

    public fooMutationInBarMutation (): void { this.fooMutation(); this.bar = 20 }
    public fooParamMutationInBarMutation (): void { this.fooParamMutation(3,4,5); this.bar = 20 }
    public setBazInMutation (): void { this.baz = 22 }
    public setGetBazInMutation (): void { this.baz = this.bar + 10 }
  }

  let store: Store
  let mutations: any[] = []

  beforeEach(() => {
    store = new Store()
    mutations = []
    store.$store.subscribe((m) => mutations.push(m))
  })

  it('generates options correctly', () => {
    expect((store as any).$__options).toStrictEqual({
      ...defaultStoreOptions,
      getters: { fooGetter: expect.any(Function), baz: expect.any(Function) },
      mutations: {
        barMutation: expect.any(Function),
        fooGetterMutation: expect.any(Function),
        fooMutation: expect.any(Function),
        fooMutationInBarMutation: expect.any(Function),
        fooParamMutation: expect.any(Function),
        fooParamMutationInBarMutation: expect.any(Function),
        setBar: expect.any(Function),
        setBaz: expect.any(Function),
        setBazInMutation: expect.any(Function),
        setFooSetter: expect.any(Function),
        setGetBazInMutation: expect.any(Function)
      },
      state: { foo: 42, bar: 42 }
    })
  })

  it('updates current state', () => {
    store.fooMutation()
    expect(store.foo).toBe(69)
    expect(store.fooGetter).toBe(69 + 10)
    expect(mutations).toEqual([{ payload: [], type: 'fooMutation' }])
  })

  it('can be commited manually', () => {
    store.$store.commit('fooMutation')
    expect(store.foo).toBe(69)
    expect(mutations).toEqual([{ payload: undefined, type: 'fooMutation' }])
  })

  it('can use other getters directly', () => {
    store.fooGetterMutation()
    expect(store.foo).toBe(42 + 10)
    expect(mutations).toEqual([{ payload: [], type: 'fooGetterMutation' }])
  })

  it('can trigger other mutations directly', () => {
    store.fooMutationInBarMutation()
    expect(store.foo).toBe(69)
    expect(store.bar).toBe(20)
    expect(mutations).toEqual([
      { payload: [], type: 'fooMutationInBarMutation' }
    ])
  })

  it('can trigger other mutations with payload directly', () => {
    store.fooParamMutationInBarMutation()
    expect(store.foo).toBe(12)
    expect(store.bar).toBe(20)
    expect(mutations).toEqual([
      { payload: [], type: 'fooParamMutationInBarMutation' }
    ])
  })

  it('can receive parameteres', () => {
    store.fooParamMutation(1, 2, 3)
    expect(store.foo).toBe(6)
  })

  it('can trigger auto-generated mutations from mutable properties', () => {
    store.bar = 69
    expect(store.bar).toBe(69)
    expect(mutations).toEqual([{ payload: [69], type: 'setBar' }])
  })

  it('bypasses auto-generated mutations from mutable properties when changing state in a mutation', () => {
    store.barMutation()
    expect(store.bar).toBe(21)
    expect(mutations).toEqual([{ payload: [], type: 'barMutation' }])
  })

  it('does not trigger auto-generated mutations when assigning the same value multiple times', () => {
    store.bar = 69
    store.bar = 69
    expect(store.bar).toBe(69)
    expect(mutations).toEqual([{ payload: [69], type: 'setBar' }])
    store.bar = 21
    store.bar = 21
    expect(store.bar).toBe(21)
    expect(mutations).toEqual([{ payload: [69], type: 'setBar' }, { payload: [21], type: 'setBar' }])
  })

  it('can be declared as a setter', () => {
    store.fooSetter = 96
    expect(store.foo).toBe(96 + 10)
  })

  it('can live alongside a getter without interference', () => {
    expect(store.baz).toBe(42)
    store.baz = 62
    expect(store.baz).toBe(62)
  })

  it('can access a setter', () => {
    store.setBazInMutation()
    expect(store.bar).toBe(22)
  })

  it('can access both the getter and setter of a given property', () => {
    expect(store.bar).toBe(42)
    store.setGetBazInMutation()
    expect(store.bar).toBe(42 + 10)
  })
})

/**
 * Mocking client-server processing
 */
const products = [
  { id: 1, title: 'iPad 4 Mini', price: 500.01, inventory: 2 },
  { id: 2, title: 'H&M T-Shirt White', price: 10.99, inventory: 10 },
  { id: 3, title: 'Charli XCX - Sucker CD', price: 19.99, inventory: 5 }
]

export interface IProduct {
  id: number
  title: string
  price: number
  inventory: number
}

export function getProducts (): Promise<IProduct[]> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(products), 100)
  })
}

export function buyProducts (_products: IProduct[]): Promise<void> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // simulate random checkout failure.
      (Math.random() > 0.5 || navigator.userAgent.indexOf('PhantomJS') > -1)
        ? resolve()
        : reject()
    }, 100)
  })
}

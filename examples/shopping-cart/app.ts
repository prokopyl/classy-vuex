import 'babel-polyfill'
import Vue from 'vue'
import App from './components/App.vue'
import { currency } from './currency'
import store from './store'

Vue.filter('currency', currency)

// tslint:disable no-unused-expression
new Vue({
  el: '#app',
  classyStore: store,
  render: (h) => h(App)
})

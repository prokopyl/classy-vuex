import { buyProducts, IProduct } from '../../api/shop'

import { Action, ClassyModule, Mutable, StoreModule } from '../../../../src'
import { Store } from '../index'

interface IInternalCartItem {
  id: number,
  quantity: number
}

export interface ICartItem {
  title: string,
  price: number,
  quantity: number
}

@StoreModule
export default class CartStore extends ClassyModule<Store> {
  @Mutable public items: IInternalCartItem[] = []
  @Mutable public checkoutStatus: null | 'successful' | 'failed' = null

  get cartProducts (): ICartItem[] {
    return this.items.map(({ id, quantity }) => {
      const product = this.$root.products.all.find((p) => p.id === id)
      return {
        title: product ? product.title : 'unknown',
        price: product ? product.price : 0,
        quantity
      }
    })
  }

  get cartTotalPrice (): number {
    return this.cartProducts.reduce((total, product) => {
      return total + product.price * product.quantity
    }, 0)
  }

  public addProductToCart (product: IProduct): void {
    this.checkoutStatus = null
    if (product.inventory > 0) {
      const cartItem = this.items.find((item) => item.id === product.id)
      if (!cartItem) {
        this.pushProductToCart(product.id)
      } else {
        this.incrementItemQuantity(product.id)
      }
      // remove 1 item from stock
      this.$root.products.decrementProductInventory(product.id)
    }
  }

  @Action public async checkout (products: IProduct[]): Promise<void> {
    const savedCartItems = this.items.slice()
    this.checkoutStatus = null
    // empty cart
    this.items = []

    try {
      await buyProducts(products)
      this.checkoutStatus = 'successful'
    } catch (e) {
      this.checkoutStatus = 'failed'
      this.items = savedCartItems
    }
  }

  private pushProductToCart (id: number): void {
    this.items.push({
      id, quantity: 1
    })
  }

  private incrementItemQuantity (id: number): void {
    const cartItem = this.items.find((item) => item.id === id)
    if (cartItem) cartItem.quantity++
  }
}

import { getProducts, IProduct } from '../../api/shop'

import { Action, ClassyModule, Mutable, StoreModule } from '../../../../src'
import { Store } from '../index'

@StoreModule
export default class ProductsStore extends ClassyModule<Store> {
  @Mutable public all: IProduct[] = []

  public decrementProductInventory (id: number): void {
    const product = this.all.find((p) => p.id === id)
    if (product) product.inventory--
  }

  @Action public async getAllProducts (): Promise<void> {
    this.all = await getProducts()
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import ClassyVuex, { ClassyStore, StoreModule } from '../../../src'

import CartStore from './modules/cart'
import ProductsStore from './modules/products'

Vue.use(Vuex)
Vue.use(ClassyVuex, { Vuex })

@StoreModule
export class Store extends ClassyStore {
  public cart = new CartStore()
  public products = new ProductsStore()
}

export default new Store()

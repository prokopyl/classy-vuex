const digitsRE = /(\d{3})(?=\d)/g

export function currency (stringValue: string, symbol: string | null, decimals: number | null): string {
  const value = parseFloat(stringValue)
  if (!isFinite(value) || (!value && value !== 0)) return ''
  symbol = symbol !== null ? symbol : '$'
  decimals = decimals !== null ? decimals : 2
  const stringified = Math.abs(value).toFixed(decimals)
  const _int = decimals
    ? stringified.slice(0, -1 - decimals)
    : stringified
  const i = _int.length % 3
  const head = i > 0
    ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
    : ''
  const _float = decimals
    ? stringified.slice(-1 - decimals)
    : ''
  const sign = value < 0 ? '-' : ''
  return sign + symbol + head +
    _int.slice(i).replace(digitsRE, '$1,') +
    _float
}

import Vue from 'vue'
import Counter from './counter.vue'
import store from './store'

// tslint:disable no-unused-expression
new Vue({
  classyStore: store,
  el: '#app',
  render: (h) => h(Counter)
})

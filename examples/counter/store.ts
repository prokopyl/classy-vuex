import Vue from 'vue'
import Vuex from 'vuex'
import ClassyVuex, { Action, ClassyStore, StoreModule } from '../../src'

Vue.use(Vuex)
Vue.use(ClassyVuex, { Vuex })

@StoreModule
export class Store extends ClassyStore {
  public count = 0

  public increment (): void {
    this.count++
  }

  public decrement (): void {
    this.count--
  }

  get evenOrOdd (): 'even' | 'odd' {
    return this.count % 2 === 0 ? 'even' : 'odd'
  }

  @Action public incrementIfOdd (): void {
    if (this.evenOrOdd === 'odd') this.increment()
  }

  @Action public async incrementAsync (): Promise<void> {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        this.increment()
        resolve()
      }, 1000)
    })
  }
}

const store = new Store()
export default store

import 'babel-polyfill'
import Vue from 'vue'
import App from './components/App.vue'
import store from './store'

(Vue.config as any).debug = true

Vue.filter('time', (timestamp: number) => {
  return new Date(timestamp).toLocaleTimeString()
})

// tslint:disable-next-line no-unused-expression
new Vue({
  el: '#app',
  classyStore: store,
  render: (h) => h(App)
})

store.getAllMessages().catch((e) => { throw e })

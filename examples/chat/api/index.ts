import data from './mock-data'
const LATENCY = 16

export interface IMessage {
  id: string,
  threadID: string,
  threadName: string,
  authorName: string,
  text: string,
  timestamp: number,
  isRead?: boolean
}

export interface IThread {
  id: string,
  name: string,
  messages: string[],
  lastMessage: IMessage | null
}

export function getAllMessages (): Promise<IMessage[]> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(data), LATENCY)
  })
}

export function createMessage (text: string, thread: IThread): Promise<IMessage> {
  const timestamp = Date.now()
  const id = 'm_' + timestamp
  const message = {
    id,
    text,
    timestamp,
    threadID: thread.id,
    threadName: thread.name,
    authorName: 'Evan'
  }
  return new Promise((resolve) => {
    setTimeout(() => resolve(message), LATENCY)
  })
}

import Vue from 'vue'
import Vuex from 'vuex'
import ClassyVuex, { Action, ClassyStore, StoreModule } from '../../src'
import * as api from './api'
import { IMessage, IThread } from './api' // tslint:disable-line no-duplicate-imports

Vue.use(Vuex)
Vue.use(ClassyVuex, { Vuex })

@StoreModule
export class Store extends ClassyStore {

  public currentThreadID: string | null = null
  public messages: {[K: string]: IMessage} = {}
  private _threads: {[K: string]: IThread} = {}

  get threads (): {[K: string]: IThread} {
    return this._threads
  }

  get currentThread (): IThread | null {
    return this.currentThreadID
      ? this.threads[this.currentThreadID]
      : null
  }

  get currentMessages (): IMessage[] {
    const thread = this.currentThread
    return thread
      ? thread.messages.map((id) => this.messages[id])
      : []
  }

  get unreadCount (): number {
    return Object.values(this.threads).reduce((count, thread) => {
      return (thread.lastMessage && thread.lastMessage.isRead) ? count : count + 1
    }, 0)
  }

  get sortedMessages (): IMessage[] {
    return this.currentMessages.slice().sort((a, b) => a.timestamp - b.timestamp)
  }

  public receiveAll (messages: IMessage[]): void {
    let latestMessage: IMessage | undefined
    messages.forEach((message) => {
      // create new thread if the thread doesn't exist
      if (!this.threads[message.threadID]) {
        this.createThread(message.threadID, message.threadName)
      }
      // mark the latest message
      if (!latestMessage || message.timestamp > latestMessage.timestamp) {
        latestMessage = message
      }
      // add message
      this.addMessage(message)
    })
    // set initial thread to the one with the latest message
    if (latestMessage) this.setCurrentThread(latestMessage.threadID)
  }

  public createThread (id: string, name: string): void {
    this.$set(this.threads, id, {
      id,
      name,
      messages: [],
      lastMessage: null
    })
  }

  public addMessage (message: IMessage): void {
    // add a `isRead` field before adding the message
    message.isRead = message.threadID === this.currentThreadID
    // add it to the thread it belongs to
    const thread = this.threads[message.threadID]
    if (!thread.messages.some((id) => id === message.id)) {
      thread.messages.push(message.id)
      thread.lastMessage = message
    }

    // add it to the messages map
    this.$set(this.messages, message.id, message)
  }

  public setCurrentThread (id: string): void {
    this.currentThreadID = id
    if (!this.threads[id]) {
      debugger // tslint:disable-line
    }
    const lastMessage = this.threads[id].lastMessage
    // mark thread as read
    if (lastMessage) lastMessage.isRead = true
  }

  @Action public async getAllMessages (): Promise<void> {
    this.receiveAll(await api.getAllMessages())
  }

  @Action public async sendMessage (text: string, thread: IThread): Promise<void> {
    this.addMessage(await api.createMessage(text, thread))
  }
}

const store = new Store()
export default store

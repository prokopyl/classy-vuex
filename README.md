Classy-Vuex is a thin wrapper around [Vuex](https://vuex.vuejs.org/),
leveraging [Typescript](https://www.typescriptlang.org/) to make your
application's stores type-safe using simple and idiomatic abstractions.

## Example snippet

Here is a short example, based on [Vuex's _counter_ example](https://github.com/vuejs/vuex/tree/v3.0.1/examples/counter).  
The full _counter_ example can be found [here](https://gitlab.com/prokopyl/classy-vuex/tree/master/examples/counter).

```typescript
@StoreModule
export class Store extends ClassyStore {
  // State
  public count = 0

  // Getter
  get evenOrOdd (): 'even' | 'odd' {
    return this.count % 2 === 0 ? 'even' : 'odd'
  }

  // Mutations
  public increment (): void {
    this.count++
  }

  public decrement (): void {
    this.count--
  }

  // Actions
  @Action public incrementIfOdd (): void {
    if (this.evenOrOdd === 'odd') this.increment()
  }

  @Action public async incrementAsync (): Promise<void> {
    return new Promise(/* ... */)
  }
}
```

Examples can be found in the `examples` directory.
You can use the `npm run examples` command to start a development server
to run the examples live on your computer.
